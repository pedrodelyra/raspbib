from threading import Thread

def print_time(name):
    print("Hello" + name)

t1 = Thread(target = print_time, args = ["Hello"])
t2 = Thread(target = print_time, args = ["Hello"])

t1.start()
t2.start()

t1.join()
t2.join()

print("Bye!")
