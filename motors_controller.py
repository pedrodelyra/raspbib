import RPi.GPIO as GPIO
import time

#                    1st motor    2nd motor    3rd motor
enable_pins     = [     11,          29,          36      ]
step_pins       = [     13,          31,          38      ]
direction_pins  = [     15,          33,          40      ]
loops_per_cm    = [    500,         100,          10      ]
frequences      = [ 0.0008,       0.001,                  ]

dy = [0, 33, 63]
dx = [9, 33, 63]

def set_pins(motor):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(enable_pins[motor],    GPIO.OUT)
    GPIO.setup(step_pins[motor],      GPIO.OUT)
    GPIO.setup(direction_pins[motor], GPIO.OUT)

def turn_on(pin):
    GPIO.output(pin, 1)
 
def turn_off(pin):
    GPIO.output(pin, 0)

def enable_motor(motor, direction, distance):
    turn_off(enable_pins[motor])
    turn_on(direction_pins[motor]) if direction else turn_off(direction_pins[motor])
    for i in range(0, distance * loops_per_cm[motor]):
        turn_on(step_pins[motor])
        time.sleep(frequences[motor])
        turn_off(step_pins[motor])
        time.sleep(frequences[motor])

def move_to(x, y):
    set_pins(0)
    set_pins(1)
    reset_gpio()

def reset_gpio():
    GPIO.cleanup()
