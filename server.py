from socket import *

rasp_ip, rasp_server_port = '192.168.10.242', 9898
webapp_ip, webapp_server_port = '192.168.10.242', 9797

rasp_server = socket(AF_INET, SOCK_STREAM)
rasp_server.bind((rasp_ip, rasp_server_port))
rasp_server.listen(1)
print('Raspberry Pi server initialized at ip', rasp_ip)
while True:
    print('Waiting for incoming connections from web application...')
    webapp_client, webapp_addr = rasp_server.accept()
    rasp_client = socket(AF_INET, SOCK_STREAM)
    rasp_client.connect((webapp_ip, webapp_server_port))
    print('Connected to web app client', webapp_addr)
    while True:
        webapp_request = webapp_client.recv(1024)
        if not webapp_request: break
        print('Request received from web app:', webapp_request)
        print('Sending response...')
        rasp_client.send('Ok! Received request: ' + webapp_request)
    print 'Finish connection with web app client', webapp_addr
    webapp_client.close
    rasp_client.close
rasp_server.close
