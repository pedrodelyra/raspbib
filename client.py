from socket import *

webapp_ip, webapp_server_port = '192.168.10.242', 9797
rasp_ip, rasp_server_port = '192.168.10.242', 9898

webapp_server = socket(AF_INET, SOCK_STREAM)
webapp_server.bind((webapp_ip, webapp_server_port))
webapp_server.listen(1)
print('Web app server initialized at ip', webapp_ip)
while True:
    print('Web application attempting to connect to raspberry pi server...')
    webapp_client = socket(AF_INET, SOCK_STREAM)
    webapp_client.connect((rasp_ip, rasp_server_port))
    print('Connected to raspberry server!')
    rasp_client, rasp_addr = webapp_server.accept()
    msg = raw_input()
    while msg <> '\x18':
        print('Sending request to Raspberry Pi')
        webapp_client.send(msg)
        rasp_response = rasp_client.recv(1024)
        print('Response received from raspberry:', rasp_response)
        msg = raw_input()
    print 'Finish connection with raspberry client', rasp_addr
    rasp_client.close
    webapp_client.close
webapp_server.close
