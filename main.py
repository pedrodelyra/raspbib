from motors_controller import *

motor, direction, distance = map(int, raw_input().strip().split(" "))
print(str(motor) + " " + str(direction) + " " + str(distance))
set_pins(motor)
enable_motor(motor, direction, distance)
reset_gpio()
